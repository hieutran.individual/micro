package micro

import "context"

// Server
type Server interface {
	Start()
	Shutdown(ctx context.Context)
}
