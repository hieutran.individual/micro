package handler

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/hieutran.individual/micro"
)

type MethodAndPath map[string]string

func (m MethodAndPath) Validate() (err error) {
	for k := range m {
		switch k {
		case http.MethodGet, http.MethodPost, http.MethodConnect, http.MethodDelete, http.MethodHead, http.MethodPatch, http.MethodOptions, http.MethodPut, http.MethodTrace:
		default:
			return fmt.Errorf("invalid method and addr variables")
		}
	}
	return nil
}

func (m MethodAndPath) Log(addr string) {
	for method, path := range m {
		micro.Log.Infof("method: %s, path: %s%s", method, addr, path)
	}
}

// Handler to invoke
type Handler interface {
	BindRoutes(r *mux.Router) MethodAndPath
}
