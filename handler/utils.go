package handler

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"gitlab.com/hieutran.individual/micro"
	"gitlab.com/hieutran.individual/micro/errors"
)

// Utils is the handler utils
type Utils struct {
	MaxRecvSize int64
}

// WalkRouter will list all path and method of the router
func WalkRouter(r *mux.Router, handlerName string) map[string]string {
	routes := map[string]string{}
	r.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		m, err := route.GetMethods()
		if err != nil {
			return err
		}
		methods := fmt.Sprint(m)
		path, err := route.GetPathTemplate()
		if err != nil {
			return err
		}
		routes[path] = string(methods)
		return nil
	})
	return routes
}

// NewUtils returns an instance of Utils
func NewUtils() *Utils {
	u := &Utils{10 << 20}
	return u
}

// DecodeJSON decode io.ReadCloser to v
func (h *Utils) DecodeJSON(w http.ResponseWriter, r io.ReadCloser, v interface{}) (err error) {
	b := http.MaxBytesReader(w, r, h.MaxRecvSize)
	return json.NewDecoder(b).Decode(v)
}

// EncodeProblemJSON encode err as problem json and bind to w.
func (h *Utils) EncodeProblemJSON(w http.ResponseWriter, err error) {
	errs := errors.FromError(err)
	w.Header().Set("Content-Type", "application/problem+json")
	h.EncodeJSON(int(errs.Code), w, errs)
}

// EncodeJSON encodes v as json format and bind to w
func (h *Utils) EncodeJSON(stt int, w http.ResponseWriter, v interface{}) {
	w.WriteHeader(stt)
	contentType := w.Header().Get("Content-Type")
	if strings.TrimSpace(contentType) == "" {
		w.Header().Set("Content-Type", "application/json")
	}
	if err := json.NewEncoder(w).Encode(v); err != nil {
		micro.Log.Errorf("cannot encode response to response writer: %s", err.Error())
	}
}

// QueryDecodeJSON returns binded request query to v using JSON encoder
func (h *Utils) QueryDecodeJSON(r *http.Request, v interface{}) (err error) {
	var schemaDecoder = schema.NewDecoder()
	return schemaDecoder.Decode(v, r.URL.Query())
}
