package server

import (
	"context"
	"crypto/tls"
	"log"
	"net"
	"net/http"
	"strings"
	"sync"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"gitlab.com/hieutran.individual/micro"
	"gitlab.com/hieutran.individual/micro/handler"
	"gitlab.com/hieutran.individual/micro/logger"
)

type server struct {
	config       *Config
	httpListener net.Listener
	httpServer   *http.Server
	serverGroup  *sync.WaitGroup
	cert         tls.Certificate
	router       *mux.Router
	handlers     []handler.Handler
}

func NewServer(opts ...Option) (micro.Server, error) {
	if micro.Log == nil {
		if err := logger.RegisterLogZap(&logger.DefaultLogging); err != nil {
			return nil, errors.Wrap(err, "establish server")
		}
	}
	srv := &server{
		config:      defaultConfig,
		httpServer:  nil,
		serverGroup: &sync.WaitGroup{},
		cert:        tls.Certificate{},
		router:      mux.NewRouter(),
		handlers:    []handler.Handler{},
	}
	for _, opt := range opts {
		if err := opt(srv); err != nil {
			return nil, errors.Wrap(err, "load option")
		}
	}
	if err := srv.setupHTTPServer(); err != nil {
		return nil, errors.Wrap(err, "setup http server")
	}
	return srv, nil
}

func (srv *server) setupHTTPServer() (err error) {
	cert, err := tls.LoadX509KeyPair(srv.config.TlsKey.CertFile, srv.config.TlsKey.KeyFile)
	if err != nil {
		micro.Log.Infof("load self signed certificate files failed, err: %s. Server will use http connection by default", err)
	}
	srv.cert = cert
	srv.httpServer = &http.Server{
		Addr:    srv.config.HttpAddr,
		Handler: srv.router,
		TLSConfig: &tls.Config{
			Certificates: []tls.Certificate{cert},
		},
	}
	var apiRouter *mux.Router
	if strings.TrimSpace(srv.config.APIPrefix) != "" {
		apiRouter = srv.router.PathPrefix(srv.config.APIPrefix).Subrouter()
		for _, apiHandler := range srv.handlers {
			bindedRoutes := apiHandler.BindRoutes(apiRouter)
			if err := bindedRoutes.Validate(); err != nil {
				return errors.Wrap(err, "bind routes")
			}
			bindedRoutes.Log(srv.httpServer.Addr)
		}
	}
	return nil
}

func (s *server) startHTTPServer() {
	s.serverGroup.Add(1)
	defer s.serverGroup.Done()
	micro.Log.Infof("server is listenning at: %s", s.httpServer.Addr)
	if err := s.httpServer.ListenAndServeTLS("", ""); err != nil {
		log.Fatalf("cannot serve http server, err: %s", err.Error())
	}
}

func (s *server) Start() {
	go s.startHTTPServer()
	s.serverGroup.Wait()
}

func (s *server) Shutdown(ctx context.Context) {
	if err := s.httpServer.Shutdown(ctx); err != nil {
		if err.Error() != http.ErrServerClosed.Error() {
			micro.Log.Errorf("an error occured while shutting down the http server, err: %s", err.Error())
		}
	}
}
