package server_test

import (
	"context"
	"log"
	"os"
	"os/signal"
	"testing"
	"time"

	"gitlab.com/hieutran.individual/micro/server"
)

func TestEstablishServer(t *testing.T) {
	srv, err := server.NewServer()
	if err != nil {
		t.Errorf("err: %s", err.Error())
	}
	go srv.Start()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	srv.Shutdown(ctx)
	log.Println("shutting down")
	os.Exit(0)
}
