package server

import (
	"gitlab.com/hieutran.individual/micro/handler"
)

type TLSKey struct {
	CertFile string
	KeyFile  string
}

type Config struct {
	AppName   string
	APIPrefix string
	TlsKey    TLSKey
	HttpAddr  string
}

var defaultConfig = &Config{
	AppName:   "application",
	APIPrefix: "api",
	TlsKey:    TLSKey{},
	HttpAddr:  "0.0.0.0:4000",
}

type Option func(s *server) (err error)

func ApplyConfig(conf *Config) Option {
	return func(s *server) (err error) {
		s.config = conf
		return nil
	}
}

func RegisterHandler(handler handler.Handler) Option {
	return func(s *server) (err error) {
		s.handlers = append(s.handlers, handler)
		return nil
	}
}
