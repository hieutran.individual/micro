package logger

import (
	"fmt"
	"net/url"

	"github.com/natefinch/lumberjack"
	"github.com/pkg/errors"
	"gitlab.com/hieutran.individual/micro"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// RegisterLogZap register zap logger to std Log
func RegisterLogZap(confs ...*LoggingConfig) (err error) {
	var l *zap.Logger
	for _, conf := range confs {
		l, err = initLogZap(conf)
		if err != nil {
			return errors.Wrap(err, "register logzap")
		}
	}
	if len(confs) == 0 {
		l, err = initLogZap(&DefaultLogging)
		if err != nil {
			return errors.Wrap(err, "register logzap")
		}
	}
	defer l.Sync()
	zlog := l.Sugar()
	micro.SetLogger(zlog)
	return nil
}

type lumberjackSink struct {
	*lumberjack.Logger
}

// Sync implements zap.Sink. The remaining methods are implemented
// by the embedded *lumberjack.Logger.
func (lumberjackSink) Sync() error { return nil }

func initLogZap(conf *LoggingConfig) (*zap.Logger, error) {
	zap.RegisterSink("lumberjack", func(u *url.URL) (zap.Sink, error) {
		return lumberjackSink{
			Logger: &lumberjack.Logger{
				Filename:   conf.Output,
				MaxSize:    conf.MaxSize,
				MaxBackups: conf.MaxBackups,
				MaxAge:     conf.MaxAge,
				Compress:   conf.Compress,
				LocalTime:  true,
			},
		}, nil
	})
	level := zap.AtomicLevel{}
	if err := level.UnmarshalText([]byte(conf.Level)); err != nil {
		return nil, errors.Wrap(err, "init log")
	}
	encoding := "json"
	if conf.Encoding == "text" {
		encoding = "console"
	}
	cfg := zap.Config{
		Level:            level,
		Development:      conf.Development,
		Encoding:         encoding,
		OutputPaths:      []string{"stdout", fmt.Sprintf("lumberjack:%s", conf.Output)},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:       "msg",
			LevelKey:         "level",
			TimeKey:          "time",
			CallerKey:        "caller",
			FunctionKey:      "func",
			NameKey:          "name",
			StacktraceKey:    "trace",
			LineEnding:       "\n",
			EncodeTime:       zapcore.TimeEncoderOfLayout(conf.TimestampFormat),
			EncodeLevel:      zapcore.LowercaseLevelEncoder,
			EncodeDuration:   zapcore.StringDurationEncoder,
			EncodeCaller:     zapcore.ShortCallerEncoder,
			ConsoleSeparator: "\t",
		},
		DisableStacktrace: true,
	}
	zlogger, err := cfg.Build()
	if err != nil {
		return nil, errors.Wrap(err, "init log")
	}
	zlogger.Debug("zap logger construction succeeded")
	return zlogger, nil
}
