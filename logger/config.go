package logger

import "fmt"

// LoggingConfig Requirement:
// Be able to switch to other log library without modifying the business code.
// No direct dependency on any log library.
// Need one global instance of the logger for the whole application, so you can
// change the log configuration in one place and apply it to the whole app.
// Can change logging behavior easily without modifying code, for example, log
// level.
// Be able to change log level dynamically when the programming is running.
type LoggingConfig struct {
	// "logrus", "zap"
	Driver loggingDriver `yaml:"driver" json:"driver"`
	// "debug", "info", "warn", "error", "dpanic", "panic", and "fatal"
	Level string `yaml:"level" json:"level"`
	// Development puts the logger in development mode, which changes the behavior
	// of DPanicLevel and takes stacktraces more liberally.
	Development bool `yaml:"development" json:"development"`
	// Encoding sets the logger's encoding. Valid values are "json" and
	// "text", as well as any third-party encodings registered via
	// RegisterEncoder.
	Encoding string `yaml:"encoding" json:"encoding"`
	// Output is the file paths to write logging output to.
	Output string `yaml:"output" json:"output"`
	// TimestampFormat sets the format used for marshaling timestamps.
	TimestampFormat string `yaml:"timestamp_format" json:"timestamp_format"`
	// MaxSize is the maximum size in megabytes of the log file before it gets
	// rotated. It defaults to 100 megabytes.
	MaxSize int `yaml:"max_size" json:"max_size"`
	// MaxBackups is the maximum number of old log files to retain.  The default
	// is to retain all old log files (though MaxAge may still cause them to get
	// deleted.)
	MaxBackups int `yaml:"max_backups" json:"max_backups"`
	// MaxAge is the maximum number of days to retain old log files based on the
	// timestamp encoded in their filename.  Note that a day is defined as 24
	// hours and may not exactly correspond to calendar days due to daylight
	// savings, leap seconds, etc. The default is not to remove old log files
	// based on age.
	MaxAge int `yaml:"max_age" json:"max_age"`
	// Compress determines if the rotated log files should be compressed
	// using gzip. The default is not to perform compression.
	Compress bool `yaml:"compress" json:"compress"`
	// LocalTime determines if the time used for formatting the timestamps in
	// backup files is the computer's local time.  The default is to use UTC
	// time.
}

// Validate validate Logging is correct or not.
func (l LoggingConfig) Validate() (err error) {
	if err = l.Driver.Validate(); err != nil {
		return err
	}
	return nil
}

type loggingDriver string

var (
	// LogrusDriver indicates logrus driver will be used to register logger.
	LogrusDriver loggingDriver = "logrus"
	// ZapDriver indicates zap driver will be used to register logger.
	ZapDriver loggingDriver = "zap"
)

func (d loggingDriver) Validate() (err error) {
	switch d {
	case LogrusDriver, ZapDriver:
		return nil
	default:
		return fmt.Errorf("invalid logging driver: %s", d)
	}
}

var (
	// DefaultLogging is the default logging configuration.
	DefaultLogging = LoggingConfig{
		"zap",
		"info",
		true,
		"json",
		"log/application.log",
		"02-01-2006 15:04:05",
		500,
		5,
		28,
		true,
	}
)
