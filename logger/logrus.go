package logger

import (
	"io"
	"os"

	"gitlab.com/hieutran.individual/micro"

	"github.com/natefinch/lumberjack"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// RegisterLogrus register logrus to std Log
func RegisterLogrus(confs ...*LoggingConfig) (err error) {
	var l *logrus.Logger
	for _, v := range confs {
		l, err = initLogrus(v)
		if err != nil {
			return errors.Wrap(err, "register logrus")
		}
	}
	if len(confs) == 0 {
		l, err = initLogrus(&DefaultLogging)
		if err != nil {
			return errors.Wrap(err, "register logrus")
		}
	}
	micro.SetLogger(l)
	return nil
}

func initLogrus(conf *LoggingConfig) (*logrus.Logger, error) {
	logger := logrus.New()
	level, err := logrus.ParseLevel(conf.Level)
	if err != nil {
		return nil, errors.Wrap(err, "init logrus")
	}
	logger.SetLevel(level)
	l := &lumberjack.Logger{
		Filename:   conf.Output,
		MaxSize:    conf.MaxSize,
		MaxBackups: conf.MaxBackups,
		MaxAge:     conf.MaxAge,
		Compress:   conf.Compress,
		LocalTime:  true,
	}
	mw := io.MultiWriter(l, os.Stdout)
	logger.SetOutput(mw)
	switch conf.Encoding {
	case "json":
		logger.SetFormatter(&logrus.JSONFormatter{
			TimestampFormat: conf.TimestampFormat,
		})
	default:
		logger.SetFormatter(&logrus.TextFormatter{
			TimestampFormat: conf.TimestampFormat,
		})
	}
	return logger, nil
}
