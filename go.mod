module gitlab.com/hieutran.individual/micro

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	go.uber.org/zap v1.16.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
